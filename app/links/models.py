from sqlalchemy import String, UniqueConstraint
from sqlalchemy.orm import Mapped, mapped_column

from app.database import Base


class LinkOrm(Base):
    __tablename__ = 'link'

    id: Mapped[int] = mapped_column(primary_key=True)
    domain: Mapped[str] = mapped_column(String(30))
    visit_timestamp: Mapped[int]

    # __table_args__ = (UniqueConstraint("domain", "visit_timestamp", name="model_num2_key"),)

