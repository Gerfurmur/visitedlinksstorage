from typing import Annotated
from time import time as tm

from fastapi import APIRouter, status, Depends, Query
from fastapi.responses import JSONResponse
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.links.models import LinkOrm as link
from app.links.schemas import LinksCreate, DomainsResponse, StandardResponse
from app.database import get_async_session

router = APIRouter(
    prefix="",
    tags=["links"],
    responses={404: {"description": "Not found"}},
)


@router.get("/visited_domains", response_model=DomainsResponse)
async def get_domains(
        *,
        _from: Annotated[int | None, Query(alias="from")] = None,
        _to: Annotated[int | None, Query(alias="to")] = None,
        session: AsyncSession = Depends(get_async_session),
):
    try:
        query = select(link.domain).order_by(link.id)

        if _from:
            query = query.where(link.visit_timestamp >= _from)

        if _to:
            query = query.where(link.visit_timestamp <= _to)

        links = await session.execute(query)
        # Выборка только уникальных значений
        links = links.unique().scalars()
        return {
            "status": "ok",
            "domains": list(links),
        }
    except Exception as exc:
        return JSONResponse(
            {
                "status": f"Internal server error: {exc}",
            },
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR
        )


@router.post("/visited_links", response_model=StandardResponse)
async def visited_links(
        links: LinksCreate,
        *,
        session: AsyncSession = Depends(get_async_session),
):
    visit_timestamp = tm()
    try:
        new_rows = []
        for url in links.links:
            domain = url.host
            new_rows.append(link(domain=domain, visit_timestamp=visit_timestamp))

        session.add_all(new_rows)
        await session.flush()
        await session.commit()
        return {
            "status": "ok",
        }
    except Exception as exc:
        await session.rollback()
        return JSONResponse(
            {
                "status": f"Internal server error: {exc}",
            },
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR
        )

