from typing import Optional
from pydantic import BaseModel, AnyUrl


class LinksCreate(BaseModel):
    links: list[AnyUrl]


class StandardResponse(BaseModel):
    status: str


class DomainsResponse(StandardResponse):
    domains: Optional[list[str]]

