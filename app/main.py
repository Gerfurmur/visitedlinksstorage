from contextlib import asynccontextmanager

from fastapi import FastAPI, status
from fastapi.exceptions import RequestValidationError
from fastapi.responses import JSONResponse

from app.links.router import router as links_router
from app import database
from app.config import settings


@asynccontextmanager
async def lifespan(app: FastAPI):
    # startup
    if settings.ENVIRONMENT == 'test':
        await database.drop_models()
    await database.init_models()
    yield
    # shutdown


app_configs = {
    "title": "Visited links",
    "description": "Store employees visited links",
    "version": "0.1",
    "lifespan": lifespan,
}

app = FastAPI(**app_configs)
app.include_router(links_router, prefix="")


@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request, exc):
    return JSONResponse(
        {
            "status": f"Bad request: {exc}",
        },
        status_code=status.HTTP_400_BAD_REQUEST
    )


# @app.on_event("startup")  # FIXME deprecated
# async def init():
#     await database.init_models()

