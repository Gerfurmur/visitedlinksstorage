import pytest
from pydantic import AnyUrl
from time import time as tm
from httpx import AsyncClient

from conftest import aclient


test_links = {
    'links': [
        'https://example.com',
        'https://example.com/again',
        'https://path.com/path',
        'ftp://query.com/path?query=param',
        'postgres://user:pass@userpass.com',
    ]
}

test_domains = set(AnyUrl(link).host for link in test_links['links'])
prev_send_timestamp = int(tm())
after_day_send_timestamp = prev_send_timestamp + 60 * 60 * 24
empty_set = set()


def update_url(url, _from, _to):
    params = {}
    if _from:
        params['from'] = _from
    if _to:
        params['to'] = _to
    params = '&'.join(f'{k}={v}' for k, v in params.items())
    return f"{url}?{params}"


async def test_add_links(aclient: AsyncClient):
    response = await aclient.post("/visited_links", json=test_links)
    assert response.status_code == 200
    assert response.json()['status'] == "ok"


async def test_add_empty_links(aclient: AsyncClient):
    response = await aclient.post("/visited_links", json={'links': []})
    assert response.status_code == 200
    assert response.json()['status'] == "ok"


async def test_get_domains(aclient: AsyncClient):
    response = await aclient.get("/visited_domains")
    assert response.status_code == 200

    data = response.json()

    assert data['status'] == "ok"
    assert set(data['domains']) == test_domains


@pytest.mark.parametrize("_from, _to, expected",
                         [
                             (1, 2, empty_set),
                             (1, 1, empty_set),
                             (3, None, test_domains),
                             (None, 2, empty_set),
                             (None, after_day_send_timestamp, test_domains),
                             (prev_send_timestamp, after_day_send_timestamp, test_domains),
                             (after_day_send_timestamp, None, empty_set),
                         ])
async def test_get_domains_with_query_params(aclient: AsyncClient, _from, _to, expected):
    url = "/visited_domains"
    url = update_url(url, _from, _to)

    response = await aclient.get(url)
    assert response.status_code == 200

    data = response.json()

    assert data['status'] == "ok"
    assert set(data['domains']) == expected


@pytest.mark.parametrize("data, status_code", [
    (
            {
                'links': ['example.com'],  # without scheme
            },
            400
    ),
    (
            {
                'l': ['example.com'],  # incorrect key
            },
            400
    ),
    (
            {
                'l': ['example.com'],  # incorrect key
                'links': [],  # correct key
            },
            200
    ),
])
async def test_bad_add_links(aclient: AsyncClient, data, status_code):
    response = await aclient.post("/visited_links", json=data)
    assert response.status_code == status_code


@pytest.mark.parametrize("_from, _to, status_code",
                         [
                             ('1', '2', 200),
                             ('1', None, 200),
                             ('1a', None, 400),
                             (None, '2', 200),
                             (None, '2a', 400),
                             ('1a', '2', 400),
                         ])
async def test_get_domains_params_validation(aclient: AsyncClient, _from, _to, status_code):
    url = "/visited_domains"
    url = update_url(url, _from, _to)

    response = await aclient.get(url)
    assert response.status_code == status_code

